/*
 * MIT License
 *
 * Copyright (c) 2021 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.discordtrellobot.configuration;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class ConfigurationFile {

    private static final ObjectMapper MAPPER = createMapper();

    private final @Nullable InputStream srcFile;
    private final @NotNull File destFile;

    public ConfigurationFile(@Nullable InputStream srcFile, @NotNull File destFile) {
        this.srcFile = srcFile;
        this.destFile = destFile;
    }

    public static ConfigurationFile fromSrcDest(@Nullable InputStream srcFile, @NotNull File destFile) {
        return new ConfigurationFile(srcFile, destFile);
    }

    public static ConfigurationFile fromDest(@NotNull File destFile) {
        return new ConfigurationFile(null, destFile);
    }

    private static ObjectMapper createMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        // General
        mapper.enable(JsonParser.Feature.ALLOW_COMMENTS); // Allow C/C++ style comments in JSON
        // Serialization
        mapper.enable(SerializationFeature.INDENT_OUTPUT); // Enable pretty-printing
        // Deserialization
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT); // Empty string "" will become null object (coercion)
        // Return-type
        return mapper;
    }

    public void copyIfNotExist() throws IOException {
        if (srcFile != null && !destFile.exists()) {
            Files.copy(this.srcFile, this.destFile.toPath());
        }
    }

    public <T> T loadJson(Class<T> tClass) throws IOException {
        return MAPPER.readValue(this.destFile, tClass);
    }
}
