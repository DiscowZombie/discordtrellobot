/*
 * MIT License
 *
 * Copyright (c) 2021 Mathéo CIMBARO
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package fr.discowzombie.discordtrellobot;

import cloud.commandframework.Description;
import cloud.commandframework.arguments.standard.StringArgument;
import cloud.commandframework.execution.CommandExecutionCoordinator;
import cloud.commandframework.jda.JDA4CommandManager;
import cloud.commandframework.jda.JDACommandSender;
import cloud.commandframework.jda.JDAGuildSender;
import com.julienvey.trello.Trello;
import com.julienvey.trello.domain.Card;
import com.julienvey.trello.impl.TrelloImpl;
import com.julienvey.trello.impl.http.ApacheHttpClient;
import fr.discowzombie.discordtrellobot.configuration.ConfigurationFile;
import fr.discowzombie.discordtrellobot.configuration.config.main.MainConfiguration;
import fr.discowzombie.discordtrellobot.configuration.config.message.MessageConfiguration;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public class Application {

    /**
     * Note that {@link GatewayIntent#GUILD_MEMBERS} is privileged
     */
    private static final EnumSet<GatewayIntent> REQUIRED_INTENTS = EnumSet.of(GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_MESSAGES);
    private static final MemberCachePolicy MEMBER_CACHE_POLICY = MemberCachePolicy.ONLINE;

    public static void main(String[] args) {
        try {
            final ConfigurationFile configFile = ConfigurationFile.fromSrcDest(
                    Application.class.getResourceAsStream("./config.json"), new File("./src/main/resources/config.json"));
            final ConfigurationFile messageFile = ConfigurationFile.fromSrcDest(
                    Application.class.getResourceAsStream("./message.json"), new File("./src/main/resources/message.json"));

            MainConfiguration mainConfiguration;
            MessageConfiguration messageConfiguration;
            try {
                configFile.copyIfNotExist();
                mainConfiguration = configFile.loadJson(MainConfiguration.class);
                messageFile.copyIfNotExist();
                messageConfiguration = messageFile.loadJson(MessageConfiguration.class);
            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }

            final Trello trello = new TrelloImpl(mainConfiguration.trello.application_key,
                    mainConfiguration.trello.access_token, new ApacheHttpClient());

            final JDA jda = JDABuilder.create(mainConfiguration.discord.token, REQUIRED_INTENTS)
                    .disableCache(EnumSet.allOf(CacheFlag.class)) // Disable all Caches
                    .setMemberCachePolicy(MEMBER_CACHE_POLICY)
                    .build();
            jda.awaitReady();

            final JDA4CommandManager<JDACommandSender> commandManager = new JDA4CommandManager<>(jda, commandSender -> ":", (sender, cmd) -> {
                final Optional<Permission> optionalPermission = Arrays.stream(Permission.values())
                        .filter(p -> p.name().equals(cmd))
                        .findFirst();
                if (sender instanceof JDAGuildSender && optionalPermission.isPresent()) {
                    return ((JDAGuildSender) sender).getMember().hasPermission(optionalPermission.get());
                }
                return true;
            }, CommandExecutionCoordinator.simpleCoordinator(), Function.identity(), Function.identity());

            commandManager.command(
                    commandManager.commandBuilder("export", Description.of("Lancer l'exportation de message en cartes Trello"))
                            .argument(StringArgument.of("listId"))
                            .argument(StringArgument.greedy("messagesId")) // Var-arg of
                            .senderType(JDAGuildSender.class)
                            .permission(Permission.ADMINISTRATOR.getName())
                            .handler(context -> {
                                final TextChannel channel = ((JDAGuildSender) context.getSender()).getTextChannel();
                                final String listId = context.get("listId");
                                final String[] messagesListString = ((String) context.get("messagesId"))
                                        .split(" ");
                                final List<Long> messagesList = toLongArray(messagesListString);

                                for (Long messageId : messagesList) {
                                    final var guildChannel =
                                            channel.getGuild().getTextChannelById(564214299531411456L);
                                    if (guildChannel == null) break;
                                    guildChannel.retrieveMessageById(messageId).queue(message -> {
                                        fromMessageToTrello(messageConfiguration, message).thenAccept(card -> {
                                            final var createdCard = trello.createCard(listId, card);
                                            channel.sendMessage(String.format("Card created at %s !",
                                                    createdCard.getShortUrl())).queue();
                                        });
                                    }, exc -> {
                                        exc.printStackTrace();
                                        channel.sendMessage("Cannot retrieve message by ID.").queue();
                                    });
                                }
                            })
            );

        } catch (LoginException | IOException | InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static List<Long> toLongArray(String[] args) {
        List<Long> longs = new ArrayList<>(8);
        for (String arg : args) {
            try {
                longs.add(Long.parseLong(arg));
            } catch (NumberFormatException e) {
                // Ignore
            }
        }
        return longs;
    }

    private static CompletableFuture<Card> fromMessageToTrello(final MessageConfiguration messageConfiguration,
                                                               final Message message
    ) {
        return CompletableFuture.supplyAsync(() -> {
            int positiveReactions = 0, negativeReactions = 0;

            try {
                final var checkEmote = message.getGuild().retrieveEmoteById(774326409354412052L).complete();
                positiveReactions = message.retrieveReactionUsers(checkEmote).complete().size();
            } catch (ErrorResponseException ere) {
                // No Emoji with provided ID found
            }

            try {
                final var crossEmote = message.getGuild().retrieveEmoteById(774326707875610654L).complete();
                negativeReactions = message.retrieveReactionUsers(crossEmote).complete().size();
            } catch (ErrorResponseException ere) {
                // No Emoji with provided ID found
            }

            final String positiveReactionsTermination = positiveReactions > 1 ? "s" : "";
            final String negativeReactionsTermination = negativeReactions > 1 ? "s" : "";
            final String desc = String.format(messageConfiguration.trelloMigrationCardDescription, String.format("%#s", message.getAuthor()),
                    positiveReactions, positiveReactionsTermination, positiveReactionsTermination,
                    negativeReactions, negativeReactionsTermination, negativeReactionsTermination);

            return createCard(message.getContentRaw(), desc);
        }).exceptionally(exc -> {
            exc.printStackTrace();
            return null;
        });
    }

    private static Card createCard(String name, String desc) {
        final Card card = new Card();
        card.setName(name);
        card.setDesc(desc);
        return card;
    }
}
